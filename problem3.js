
function sortByAlphabeticalOrder(inventory) {
    const name = [];
    if (Array.isArray(inventory)) {
        for (car of inventory) {

            name.push(car.car_make);
        }
        name.sort();
    }
    else{
        return [];
    }

    return name;
}


module.exports = sortByAlphabeticalOrder;