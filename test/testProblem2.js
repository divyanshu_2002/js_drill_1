const findLastCar = require('./problem2');
const inventory = require('./inventory.json');

try {
    const result = findLastCar(inventory);
    if (result) {
        console.log(`The details of last car is`);
        console.log(result);
    }
    else {
        console.log(`There is no car in the inventory`);
    }
}
catch (error) {
    console.log(error.message);
}


