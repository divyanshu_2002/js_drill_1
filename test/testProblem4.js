const getCarYears = require('./problem4');
const inventory = require('./inventory.json');

try{
   let carYears=getCarYears(inventory);
   if(carYears){
       console.log(`The car years are`);
       console.log(carYears);
   }
   else{
       console.log(`There are no cars in the inventory`);
   }
}
catch(error){
    console.log(error.message);
}
