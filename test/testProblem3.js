const inventory=require("./inventory.json");
const sortByAlphabeticalOrder=require("./problem3.js");

try{
    let nameOfCars=sortByAlphabeticalOrder(inventory);
    if(nameOfCars){
        console.log(`The car names that are in inventory in sorted order are`);
        console.log(nameOfCars);
    }
    else{
        console.log(`There are no cars in the inventory`);
    }
}
catch(error){
    console.log(error.message);
}