function infoOfLastCar(inventory) {
    if (Array.isArray(inventory) && inventory.length > 0) {
        return inventory[inventory.length - 1];
    }
    else {
        return [];
    }

}

module.exports = infoOfLastCar;