const inventory = require("./inventory.json")
function infoOfCarWithId(inventory, id) {
    if (Array.isArray(inventory)) {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id == id) {
                return inventory[index];
            }
        }
    }
    else{
        return [];
    }

}

module.exports = infoOfCarWithId;