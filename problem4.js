
function allYearOfCar(inventory) {
    const year = [];
    if (Array.isArray(inventory)) {
        for (car of inventory) {
            year.push(car.car_year);
        }
        return year;
    }
    else {
        return year;
    }
}

module.exports = allYearOfCar;