const years = require("./problem4");
console.log(years);
function carOlderThan2000() {
  let noOfCar = 0;
  if (Array.isArray(years)) {
    for (year of years) {
      if (year < 2000) {
        noOfCar++;
      }
    }
    return noOfCar;
  }
  else {
    return null;
  }

}

module.exports = carOlderThan2000;